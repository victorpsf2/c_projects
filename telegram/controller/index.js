const Util = require("./util.js");

class Controller extends Util {
  constructor() {
    super();
  }

  async onMessage(snap = this.options.telegram_message) {
    var response = await this.eventToPromise({ eventName: 'find-chatId', data: { chat_id: snap.chat.id }, returnCallback: true });
    await this.bot.sendMessage(snap.chat.id, snap.text);
    return true;
  }

  async listen() {
    this.bot.on("message", async (snap) => {
      try {
        return await this.onMessage(snap);
      }
      catch (error) {
        this.emit("error", {
          error: error,
          date: new Date
        });
      }
    });
  }
}

module.exports = Controller;
