const Config = require("../kernel/config.js");

class Util extends Config {
  constructor() {
    super();
    this.on('schedule', this.schedule);
  }

  async schedule() {
    let date = new Date();

    if (
      date.toLocaleTimeString() != '8:30:00'
    ) return true;

    let result = await this.eventToPromise({ eventName: "get-allChatId", returnCallback: true });

    if (result.length) {
      let listError = ((result) => {
        let errors = [];
        result.forEach((chat_id) => {
          try {
            this.bot.sendMessage(chat_id, 'hora do pão nosso!');
          } catch (error) {
            errors.push({ chat_id: chat_id, time: new Date(), error: error });
          }
        });

        return errors;
      })(result);
    }
  }
}

module.exports = Util;
