class Model {
  constructor() {
    this.message_id = new Number;
    this.from = {
      id: new Number,
      is_bot: new Boolean,
      first_name: new String,
      last_name: new String,
      language_code: new String
    };
    this.chat = {
      id: new Number,
      first_name: new String,
      last_name: new String,
      type: new String
    };
    this.date = new Number;
    this.text = new String;
  }
}

module.exports = new Model;