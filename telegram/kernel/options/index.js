module.exports = {
  telegram_message: require('./telegram_message'),
  event_to_promise: require('./eventToPromise'),
  find_id: (new (class Model { chat_id = new Number(); })),
  error_event: require('./errorLogOptions.js')
}
