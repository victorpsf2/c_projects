const { EventEmitter } = require("events");

class Event extends EventEmitter {
  constructor() {
    super();
    this.arrayStorage = [];
    this.objectStoragr = {};
    this.options = require('./options');
    this.on('get-allChatId', this.getAllChatId);
    this.on('find-chatId', this.findChatId);
    this.on("error", this.logError);
  }

  async findChatId(opt = this.options.find_id, callback) {
    try {
      let chats = await this.eventToPromise({ eventName: 'get-allChatId', returnCallback: true });

      let index = chats.indexOf(opt.chat_id);
      if (index >= 0) {
        return callback(null, chats[index]);
      }

      this.eventToPromise({ eventName: 'save-user', data: { chat_id: opt.chat_id } })
      return callback(null, null);
    } catch (error) {
      return callback(error, null);
    }
  }

  eventToPromise(opt = this.options.event_to_promise) {
    return new Promise((resolve, reject) => {
      try {
        if (opt.returnCallback) {
          if (opt.data) {
            this.emit(opt.eventName, opt.data, (error, result) => {
              if (error) return reject(error);
              return resolve(result);
            });
          } else {
            this.emit(opt.eventName, (error, result) => {
              if (error) return reject(error);
              return resolve(result);
            });
          }
        } else {
          if (opt.data) {
            this.emit(opt.eventName, opt.data);
            return resolve(true);
          } else {
            this.emit(opt.eventName);
            return resolve(true);
          }
        }
      } catch (error) {
        return reject(error);
      }
    });
  }

  async logError(opt) {
  }
}

module.exports = Event;
