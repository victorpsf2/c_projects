const Event = require("./event.js");

class Dependecy extends Event {
  constructor() {
    super();
    this.fs = require("fs");
    this.crypto = require("crypto");
    this.core = require("node-telegram-bot-api");
    this.dotenv = require("dotenv");
    this.bot = null;
    this.on('save-user', this.saveUser);
  }

  async dataBase() {
    let dir = await this.fs.readdirSync("./");
    if (dir.indexOf('database') < 0)
      await this.fs.mkdirSync('./database');
    dir = await this.fs.readdirSync('./database/');
    if (dir.indexOf('users') < 0)
      await this.fs.mkdirSync('./database/users');
    return true;
  }

  async saveUser(opt = this.options.find_id) {
    let fileName = opt.chat_id + '.json';
    await this.fs.writeFileSync("./database/users/" + fileName, JSON.stringify({}));
    return true;
  }

  async getAllChatId(callback) {
    try {
      let ids = [];
      let files = await this.fs.readdirSync('./database/users/');

      for (let x = 0; x < files.length; x++) {
        let fileName = files[x];

        if (!(/[0-9]*\.json/g.test(fileName)))
          continue;

        let id = parseInt(fileName.replace(/\.json/g, ''));
        if (id) ids.push(id);
      }

      return callback(null, ids);
    } catch (error) {
      return callback(error, null);
    }
  }

  async newBot() {
    this.bot = new (this.core)(process.env.APITOKEN, {
      polling: (process.env.POLLING == "true") ? true : false
    });
  }
}

module.exports = Dependecy;
