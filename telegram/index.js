const Controller = require("./controller");

class Main extends Controller {
  constructor() {
    super();
    this.dotenv.config();
  }

  async build() {
    await this.newBot();
    await this.dataBase();
    this.listen();
    setInterval(async () => {
      this.emit("schedule");
    }, parseInt(process.env.TIMEINTERVAL) || 3600000);
  }

}

(new Main).build();
